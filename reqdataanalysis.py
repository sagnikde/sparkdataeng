from pyspark.sql import SparkSession
from pyspark import SparkContext
import math
import sys

from pyspark.sql.functions import udf
from pyspark.sql.types import *




#This calculates the distance (in km) between two places assuming Earth is a perfect sphere
#See https://stackoverflow.com/questions/27928/calculate-distance-between-two-latitude-longitude-points-haversine-formula
#Also https://en.wikipedia.org/wiki/Haversine_formula
def dist(lat1,lon1,lat2,lon2):
    
    def toRad(deg):
        return deg*math.pi/180
    #Mean radius of eath
    R = 6371
    #Convert to radians
    lat1r = toRad(lat1)
    lon1r = toRad(lon1)
    lat2r = toRad(lat2)
    lon2r = toRad(lon2)
    #Computing differences in latitude and longitude
    dlat = lat1r-lat2r
    dlon = lon1r-lon2r
    a = math.pow(math.sin(dlat/2),2)+math.cos(lat1r)*math.cos(lat2r)*math.pow(math.sin(dlon/2),2)
    return R*2*math.asin(math.sqrt(a))

#Iterates through a POI list and finds the one with minimum distance 
def findlabel(locationlat,locationlon,poilist):
    distances = map(lambda poilistrow : (dist(locationlat,locationlon,poilistrow[1],poilistrow[2]),poilistrow[0]),poilist)
    #I'll take advantage of how python orders tuples.
    mindist = min(distances)
    return mindist[1]

#Returns a dataframe with the requests labelled. 
def labellocations(locationsdf,poilist):
    label_udf = udf(lambda lat,lon : findlabel(lat,lon,poilist))
    labelledlocations = locationsdf.withColumn('label',label_udf(locationsdf.Latitude,locationsdf.Longitude))
    return labelledlocations

# This function performs all the necessary analysis regarding distance.
# The distance is recalculated after performing an join with the POIs to get the corresponding latitude and logitude.
# The distance is calculated.
# All the operations are then done in a single SQL statement.
# The radius is the maximum distance of a POI from a request. Obviously it will cover all the requests.
# If a POI is not in one of the labels then an outer  join is performed to include that POI in. 
def analysis(sparksess,labelledlocationDf,poilistDf):
    #I'll perform a join with POIID, however I need to avoid a clash of column names. Also there is a space in front of Latitude
    renamedpoilist = poilistDf.withColumnRenamed(' Latitude','plat').withColumnRenamed('Longitude','plon')
    #The null values are refilled with nan before calculating the distance.
    locandpoi = labelledlocationDf.join(renamedpoilist,labelledlocationDf.label==renamedpoilist.POIID)
    distance_udf = udf(dist,DoubleType())
    #The distance is recalculated 
    locationanddistances = locandpoi.withColumn('Distance',distance_udf(locandpoi.Latitude,locandpoi.Longitude,locandpoi.plat,locandpoi.plon))
    #Only the POIID and the distance of the requests are needed for analysis.
    poianddist = locationanddistances.select(['label','Distance'])
    poianddist.createOrReplaceTempView("POIDist")
    #Spark SQL supports all the required functions and so I calculated them is a single statement                        
    actualtable = sparksess.sql("select label,count(*) as req_count, max(Distance) as radius,avg(Distance) as mean_dist,stddev(Distance) as dev_dist from POIDist group by label")
    #Calculating the density
    tablewithdensity = actualtable.withColumn("Density",actualtable.req_count/(math.pi*actualtable.radius*actualtable.radius))

    #Include the unlabelled POIs
    POIIDS = poilistDf.select('POIID')
    newtable = POIIDS.join(tablewithdensity,POIIDS.POIID==tablewithdensity.label,'left_outer').drop('label').na.fill(float('nan'))
    return newtable
    

# I'll attempt to model the POIs. Its is very hard to do with mean and standard deviation. Especially if the deviations exceed the mean.
# A better option was to use the quartile functions and to test how many requests are from 25,50 and 75 quartile distances.
# In this case I'll use the beta distribution defined with parameters 'a' and 'b' as the distribution of the requests.
# The standard deviation is sd = sqrt[ a b /((a+b)^2 *(a+b+1))]
# The mean is m = a/(a+b)
# The median is med = (a-1/3)/(a+b-2/3)
# Then we have {med = (-3*m^2 + 3*m^3 + sd^2 + 3*m*sd^2)/(-3*m + 3*m^2 + 5*sd^2)}
# I'll calculate median density i.e. req_count/(med^2)
# The POIs will be mapped to an range of -10 to 10 in part of their importance according to median density

def modelPOI(analysedPOI):
    def mediandensity(count,mean,sd):
        m = mean
        med = (-3*m*m+3*m*m*m+sd*sd+3*m*sd*sd)/(-3*m + 3*m*m + 5*sd*sd)
        #Avoid the divide by zero error
        return count/(med*med+0.000001)
    weightedPOI_udf = udf(mediandensity,DoubleType())
    weightedPOI = analysedPOI.withColumn("Weight",weightedPOI_udf(analysedPOI.req_count,analysedPOI.mean_dist,analysedPOI.dev_dist)).select(['POIID','Weight']).na.fill(0)
    maxweight = weightedPOI.agg({"Weight":"max"}).collect()[0][0]
    minweight = weightedPOI.agg({"Weight":"min"}).collect()[0][0]
    modelledPOI = weightedPOI.withColumn("Grade",20*(weightedPOI.Weight - minweight)/(maxweight-minweight)-10)
    return modelledPOI



#This just sends the appropiate dataframes to the appropiate functions
#Here the locations are labelled, analyzed and then modelled
def getresults(sparksess,reqlocationdf,poilistdf):
    ss = sparksess
    poilist = list(poilistdf.rdd.map(lambda row : (row[0],row[1],row[2])).collect())
    labelled = labellocations(reqlocationdf,poilist)
    analyseddf = analysis(ss,labelled,poilistdf)
    modelleddf = modelPOI(analyseddf)
    return (labelled,analyseddf,modelleddf)


#Aquires the data from the appropiate CSV files and outputs the results
def runsparksession(reqfile,poifile,outputdir,exectype):
    ss = SparkSession.builder.appName("Sample Data").config("spark.some.config.option", "some-value").getOrCreate()
    reqlocationdf = ss.read.csv(reqfile,header=True,inferSchema=True)
    poilistdf = ss.read.csv(poifile,header=True,inferSchema=True)
    #Cleaning the dataframe of all the duplicates.
    #Apparently there is a space in front of TimeSt in the csv file
    cleanedsample = reqlocationdf.dropDuplicates([' TimeSt','Latitude','Longitude'])
    sampleddata = cleanedsample.limit(1) if exectype == 0 else (cleanedsample.sample(True,0.01) if exectype == 1  else cleanedsample)
    result = getresults(ss,sampleddata,poilistdf) 
    result[0].write.csv(outputdir + "labelled.csv")
    # To have a single file instead of multiple parts
    # result[1].coalesce(1).write.csv(outputdir + "analysed.csv") 
    result[1].write.csv(outputdir + "analysed.csv")
    result[2].write.csv(outputdir + "model.csv")
    ##print(result[2])
    ss.stop()



if __name__ == "__main__":
    reqfile = sys.argv[1]
    poifile = sys.argv[2]
    output = sys.argv[3]
    # 0 -> 1 row only
    # 1 -> 1% of the input
    # any other number, the whole DataSample file.
    runsparksession(reqfile,poifile,output,3)
    
