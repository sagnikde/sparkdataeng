from pyspark.sql import SparkSession
from pyspark import SparkContext
import math


from pyspark.sql.functions import udf
from pyspark.sql.types import *
from pyspark.sql import *

ss = SparkSession.builder.master("local[4]").appName("Sample Data").config("spark.some.config.option", "some-value").getOrCreate()
#sqlCon = SQLContext(ss)
sc = ss.sparkContext
numrdd = sc.parallelize(list(range(10)))
numrddn = numrdd.map(lambda x : Row(Numb=x))
numdf = ss.createDataFrame(numrddn)
numdf.createOrReplaceTempView("numsql")
sqres = ss.sql("select count(*),avg(*),stddev(*) from numsql")
print(sqres.collect())
ss.stop()